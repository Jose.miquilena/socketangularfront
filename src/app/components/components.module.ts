import { NgModule } from "@angular/core";
import { MessagesComponent } from './messages/messages.component';
import { Keyboard } from '@ionic-native/keyboard/ngx';
import { IonicModule } from '@ionic/angular';
import { CommonModule } from '@angular/common';

@NgModule({
    imports:[
        IonicModule,
        CommonModule
    ],
    declarations:[
        MessagesComponent
    ],
    exports:[
        MessagesComponent
    ],
    providers: [Keyboard]
})
export class GeneralComponentsModule{}