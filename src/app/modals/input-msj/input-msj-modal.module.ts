import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { InputMsjPageModal } from './input-msj-modal';
 
import { FlexLayoutModule } from '@angular/flex-layout'
const routes: Routes = [
  {
    path: '',
    component: InputMsjPageModal
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    FlexLayoutModule
  ],
  declarations: [InputMsjPageModal],
  entryComponents: [InputMsjPageModal]
})
export class InputMsjPageModalModule {}
