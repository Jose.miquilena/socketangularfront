import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KioskMensajesPage } from './kiosk-mensajes.page';

describe('KioskMensajesPage', () => {
  let component: KioskMensajesPage;
  let fixture: ComponentFixture<KioskMensajesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KioskMensajesPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KioskMensajesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
