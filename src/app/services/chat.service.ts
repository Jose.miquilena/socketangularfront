import { Injectable } from '@angular/core';

import { WebsocketService } from '../shared/websocket/websocket.service';
import { Observable } from 'rxjs';



@Injectable({
  providedIn: 'root'
})
export class ChatService {

  
  constructor(private wsService: WebsocketService) { }

/**
   * Sends a message to a chatroom
   * @param message The message to send
   */
  sendMessage(message): void {
    this.wsService.emit('message', message);
  }  

  /**
   * Subscribes to incoming message
   */
  onMessage(): Observable<any> {
    return this.wsService.on('message');
  }

 onConnected(kioskName): Observable<any>{
   return this.wsService.on(kioskName);
 }


  getKioskos():Observable<any> {
    return this.wsService.on('kioskos');
  }

  onMymsj():Observable<any>{
    let event=localStorage.getItem('kiosko');
    return this.wsService.on('messageKiosko');
  }

  onNotificationPromo():Observable<any>{
    return this.wsService.on('promo');
  }

  sendMessage2(message,kiosko): void {
    this.wsService.emit('messageKiosko', { message, kiosko });
  }

}
