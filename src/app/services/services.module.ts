import { NgModule, ModuleWithProviders } from "@angular/core";
import { HttpClientModule } from '@angular/common/http';
import { MessagesService } from './messages.service';


@NgModule({
    imports:[HttpClientModule]
})
export class ServicesModule{
    static forRoot(): ModuleWithProviders{
        return {
            ngModule: ServicesModule,
            providers:[
                MessagesService
            ]
        };
    }
}