export class ReceiveChat{
    static readonly type = '[chat] Receive chat';
    constructor(){}
}

export class GetUsers{
    static readonly type ='[chat] Get Users';
    constructor(){}
}

export class SendChat{
    static readonly type = '[chat] Send chat';
    constructor(public message:string){}
}