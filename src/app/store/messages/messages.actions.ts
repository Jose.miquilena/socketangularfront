export class GetMessages{
    static readonly type = '[Messages] Get All';
    constructor(){}
}
export class GetMessagesId{
    static readonly type = '[Messages] Get Message for Id';
    constructor(public id:number){}
}