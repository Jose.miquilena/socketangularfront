import { MessagesStateModel } from './messages.state.model';
import { State, Store, Action, StateContext } from '@ngxs/store';
import { MessagesService } from 'src/app/services/messages.service';
import { GetMessages, GetMessagesId } from './messages.actions';

@State<MessagesStateModel>({
    name: 'messages',
    defaults: {
        messages: null,
        mesaggeId:null
    }
})
export class MessagesState {
    constructor(private store: Store, private MessagesService:MessagesService) { }

    
    @Action(GetMessages)
    async getMessages(ctx: StateContext<MessagesStateModel>, action: GetMessages) {
        try {

            
            const messages:any= await this.MessagesService.getMessages();
            
            ctx.patchState({
                messages
            });

        } catch (error) {
            console.log('Error: ',String(error));
        }
    }

    
    @Action(GetMessagesId)
    async getMessagesId(ctx: StateContext<MessagesStateModel>, action: GetMessagesId) {
        const { id } = action;
        const mesaggeId:any= await this.MessagesService.getMessageId(id);
        ctx.patchState({
            mesaggeId
        })   
       
    }



}